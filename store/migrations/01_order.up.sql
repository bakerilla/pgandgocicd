CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE TABLE orders(
   id uuid DEFAULT uuid_generate_v4(),
   amount DECIMAL(36,18),
   created_at timestamptz default now(),
   PRIMARY KEY(id)
);
