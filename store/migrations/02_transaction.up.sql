CREATE TABLE transaction(
   id uuid,
   order_id uuid,
   amount DECIMAL(18,18),
   action varchar,
   date_created timestamptz,
   PRIMARY KEY( id)
);
