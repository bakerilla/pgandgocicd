package store

import (
	"database/sql"
	"embed"
	"fmt"
	"log"
	"net/http"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"

	// "github.com/golang-migrate/migrate/v4/source/iofs"
	"github.com/golang-migrate/migrate/v4/source/httpfs"

	"github.com/spf13/cobra"
)

func MigrateCommands(db *sql.DB) []*cobra.Command {

	return []*cobra.Command{
		MigrateUpCommand(db),
		MigrateDownCommand(db),
	}
}

//go:embed migrations/*.sql
var fs embed.FS

func MigrateUpCommand(db *sql.DB) *cobra.Command {
	return &cobra.Command{
		Use:   "migrate-up",
		Short: "UPs a migration",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("migrate up")
			// https://github.com/golang-migrate/migrate/issues/471
			source, err := httpfs.New(http.FS(fs), "migrations")
			// d, err := iofs.New(fs, "migrations/*.sql")
			if err != nil {
				log.Fatal("byebye", err)
			}
			driver, err := postgres.WithInstance(db, &postgres.Config{})
			if err != nil {
				log.Panic("migrate-up initialization(driver) error", err)
			}

			migration, err := migrate.NewWithInstance("httpfs", source, "pg", driver)
			if err != nil {
				log.Panic("migrate-up initialization(migrator) error", err)
			}

			if err := migration.Up(); err != nil && err != migrate.ErrNoChange {
				log.Panic("migrate-up error", err)
			}
			ver, dirty, _ := migration.Version()
			fmt.Printf("end migration ver[%v] dirty[%v] ", ver, dirty)

		},
	}
}
func MigrateDownCommand(db *sql.DB) *cobra.Command {
	return &cobra.Command{
		Use:   "migrate-down",
		Short: "DOWNs a migration",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("migrate down")
			// https://github.com/golang-migrate/migrate/issues/471
			source, err := httpfs.New(http.FS(fs), "migrations")
			// d, err := iofs.New(fs, "migrations/*.sql")
			if err != nil {
				log.Fatal("byebye", err)
			}
			driver, err := postgres.WithInstance(db, &postgres.Config{})
			if err != nil {
				log.Panic("migrate-down initialization(driver) error", err)
			}

			migration, err := migrate.NewWithInstance("httpfs", source, "pg", driver)
			if err != nil {
				log.Panic("migrate-down initialization(migrator) error", err)
			}

			if err := migration.Down(); err != nil && err != migrate.ErrNoChange {
				log.Panic("migrate-down error", err)
			}
			ver, dirty, _ := migration.Version()
			fmt.Printf("end migration-down ver[%v] dirty[%v] ", ver, dirty)

		},
	}
}
