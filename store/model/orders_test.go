package model_test

import (
	"context"
	"database/sql"

	"github.com/gofrs/uuid"
	. "github.com/onsi/ginkgo/v2"
	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
	"github.com/uptrace/bun"

	"pgandgocicd/store/model"

	_ "github.com/jackc/pgx/v5/stdlib"
)

var _ = Describe("Orders", func() {
	var (
		ctx = context.TODO()
		tx  bun.Tx
	)

	BeforeEach(func() {
		var err error
		tx, err = bunDB.BeginTx(ctx, &sql.TxOptions{})
		assert.NoError(GinkgoT(), err)

	})
	AfterEach(func() {
		assert.NoError(GinkgoT(), tx.Rollback())

	})

	Context("Insert and Select", func() {
		var (
			amount, _ = decimal.NewFromString("100.21")
			order     = &model.Orders{
				Amount: amount,
			}
			uuidString string
		)

		It("Insert order and select", func() {
			_, err := tx.NewInsert().Model(order).Exec(ctx)
			assert.NoError(GinkgoT(), err)
			uuidString = order.ID.String()

			_, err = uuid.FromString(uuidString)
			assert.NoError(GinkgoT(), err)
			// Select order of UUIDstring"
			selectedOrder := new(model.Orders)
			err = tx.NewSelect().Model(selectedOrder).Where("id = ?", uuidString).Scan(ctx)
			assert.NoError(GinkgoT(), err)

			assert.Equal(GinkgoT(), amount.String(), selectedOrder.Amount.String())
		})
	})

})
