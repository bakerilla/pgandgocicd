package model_test

import (
	"database/sql"
	"os"
	"pgandgocicd/db"
	"testing"

	"github.com/jackc/pgx/v5"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/pgdialect"
)

var (
	bunDB *bun.DB
)

func TestModel(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Model Suite")
}

var _ = BeforeSuite(func() {
	cf := db.Config{
		User:     os.Getenv("POSTGRES_USER"),
		Password: os.Getenv("POSTGRES_PASSWORD"),
		Hosts: []string{
			os.Getenv("POSTGRES_HOST"),
		},
		Ports: []string{
			os.Getenv("POSTGRES_PORT"),
		},
		DBName:  os.Getenv("POSTGRES_DB"),
		SSLMode: "disable",
	}

	dbDSN := cf.ConnectionString(db.ConnectionStringDSN)

	if _, err := pgx.ParseConfig(dbDSN); err != nil {
		panic(err)
	}
	DB, err := sql.Open("pgx", dbDSN)
	if err != nil {
		panic(err)
	}
	if err := DB.Ping(); err != nil {
		panic(err)
	}
	bunDB = bun.NewDB(DB, pgdialect.New())

	if err := DB.Ping(); err != nil {
		panic(err)

	}
})

var _ = AfterSuite(func() {
	bunDB.Close()
})
