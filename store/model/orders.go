package model

import (
	"time"

	"github.com/gofrs/uuid"
	"github.com/shopspring/decimal"
	"github.com/uptrace/bun"
)

type Orders struct {
	bun.BaseModel `bun:"table:orders,alias:o"`
	ID            uuid.UUID       `bun:"id,pk,type:uuid,default:uuid_generate_v4()"`
	Amount        decimal.Decimal `bun:"amount,type:decimal(18,18)"`
	CreatedAt     time.Time       `bun:",notnull,default:current_timestamp"`
}
