package db_test

import (
	"os"

	"github.com/jackc/pgx/v5"
	"github.com/jmoiron/sqlx"
	. "github.com/onsi/ginkgo/v2"
	"github.com/stretchr/testify/assert"

	_ "github.com/jackc/pgx/v5/stdlib"

	"pgandgocicd/db"
)

var _ = Describe("Config", func() {
	Context("validate connectionstring against", func() {
		cf := db.Config{
			User:     os.Getenv("POSTGRES_USER"),
			Password: os.Getenv("POSTGRES_PASSWORD"),
			Hosts: []string{
				os.Getenv("POSTGRES_HOST"),
			},
			Ports: []string{
				os.Getenv("POSTGRES_PORT"),
			},
			DBName:  os.Getenv("POSTGRES_DB"),
			SSLMode: "disable",
		}

		It("validate URI, connects using SQLX", func() {
			str := cf.ConnectionString(db.ConnectionStringURI)
			_, err := pgx.ParseConfig(str)
			assert.NoError(GinkgoT(), err)

			db, err := sqlx.Open("pgx", str)
			assert.NoError(GinkgoT(), err)
			assert.NoError(GinkgoT(), db.Ping())

		})
		It("validate DSN", func() {
			str := cf.ConnectionString(db.ConnectionStringDSN)
			_, err := pgx.ParseConfig(str)
			assert.NoError(GinkgoT(), err)

			db, err := sqlx.Open("pgx", str)
			assert.NoError(GinkgoT(), err)
			assert.NoError(GinkgoT(), db.Ping())

		})
	})

})
