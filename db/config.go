package db

import (
	"fmt"
	"strings"
)

type interfacer interface {
	ConnectionString(strType ConnectionStringType) string
}

type ConnectionStringType string

const (
	ConnectionStringDSN ConnectionStringType = "DSN"
	ConnectionStringURI ConnectionStringType = "URI"
)

var _ interfacer = (*Config)(nil)

type Config struct {
	User     string
	Password string
	Hosts    []string
	Ports    []string
	DBName   string
	SSLMode  string //disable for localhost
}

func (me Config) ConnectionString(in ConnectionStringType) string {
	switch in {
	case ConnectionStringDSN:
		return me.dsn()
	case ConnectionStringURI:
		return me.uri()
	}
	return me.dsn()
}

func (me Config) uri() string {
	var (
		host, port string
	)
	// postgres://jack:secret@pg.example.com:5432/mydb?sslmode=verify-ca
	if hosts := me.Hosts; len(hosts) > 0 {
		host = hosts[0]
	}
	if ports := me.Ports; len(ports) > 0 {
		port = ports[0]
	}
	uri := `postgres://%s:%s@%s:%s/%s?sslmode=%s`
	return fmt.Sprintf(uri, me.User, me.Password, host, port, me.DBName, me.SSLMode)
}

//
func (me Config) dsn() string {
	/* user=jack
	password=secret
	host=host1,host2,host3
	port=5432,5433,5434
	dbname=mydb
	sslmode=verify-ca
	*/
	dsn := `user=%s password=%s host=%s port=%s dbname=%s sslmode=%s`
	out := fmt.Sprintf(dsn,
		me.User,
		me.Password,
		strings.Join(me.Hosts, ","),
		strings.Join(me.Ports, ","),
		me.DBName,
		me.SSLMode,
	)
	return out
}
