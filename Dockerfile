FROM golang:1.19.2-alpine3.15 AS builder

WORKDIR /build

COPY . .

RUN go version
RUN go build -o appbin 


WORKDIR /dist

RUN cp /build/appbin .
RUN cp -r /build/store/migrations .

FROM alpine:latest

WORKDIR /app
COPY --from=builder /dist .
CMD ["/app/appbin"]