.EXPORT_ALL_VARIABLES:
include .env
export

test: clean
	go test ./... -race -v

test_ginkgo: clean
	ginkgo test ./... -race -v

clean: 
	@go clean -testcache
run:
	@go run *.go
run_migrate:
	@go run *.go migrate-up

run_migrate_down:
	@go run *.go migrate-down

run_help:
	@go run *.go help