package main

import (
	"database/sql"
	"fmt"
	"os"
	"pgandgocicd/db"
	"pgandgocicd/store"

	"github.com/jackc/pgx/v5"
	_ "github.com/jackc/pgx/v5/stdlib"
	"github.com/spf13/cobra"
	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/pgdialect"
)

var (
	pgDB  *sql.DB
	bunDB *bun.DB
)

func init() {
	// main body
	cf := db.Config{
		User:     os.Getenv("POSTGRES_USER"),
		Password: os.Getenv("POSTGRES_PASSWORD"),
		Hosts: []string{
			os.Getenv("POSTGRES_HOST"),
		},
		Ports: []string{
			os.Getenv("POSTGRES_PORT"),
		},
		DBName:  os.Getenv("POSTGRES_DB"),
		SSLMode: "disable",
	}

	dbDSN := cf.ConnectionString(db.ConnectionStringDSN)

	if _, err := pgx.ParseConfig(dbDSN); err != nil {
		panic(err)
	}
	DB, err := sql.Open("pgx", dbDSN)
	if err != nil {
		panic(err)
	}
	if err := DB.Ping(); err != nil {
		panic(err)
	}
	pgDB = DB
	bunDB = bun.NewDB(pgDB, pgdialect.New())

}

func main() {
	var rootCmd = &cobra.Command{
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println(pgDB)
		},
	}

	//migrations
	rootCmd.AddCommand(store.MigrateCommands(pgDB)...)

	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintf(os.Stderr, "Whoops. There was an error while executing your CLI '%s'", err)
		os.Exit(1)
	}

}
